// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QEUnit.h"
#include "Unit_Twiglet.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API AUnit_Twiglet : public AQEUnit
{
	GENERATED_BODY()

public:
	AUnit_Twiglet();
};

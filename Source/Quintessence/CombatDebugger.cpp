// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CombatDebugger.h"
#include "CombatMap.h"
#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "QEUnit.h"

ACombatDebugger::ACombatDebugger()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> UIClassFinder(TEXT("/Game/ThirdPersonCPP/CombatUI/CombatDebuggerUI"));;
	if (UIClassFinder.Succeeded())
	{
		UIClass = UIClassFinder.Class;
	}
}

void ACombatDebugger::BeginPlay()
{
	Super::BeginPlay();
	if (UWorld* World = GetWorld())
	{
		PlayerUI = CreateWidget(World, UIClass, FName("DebuggerHUD"));
		PlayerUI->AddToViewport();
	}
}

void ACombatDebugger::AddUnit(int TeamIndex, AQEUnit* Unit)
{
	if (TeamIndex == 0)
	{
		TeamOneArr.Add(Unit);
	}
	else
	{
		TeamTwoArr.Add(Unit);
	}
}

void ACombatDebugger::StartCombat()
{
	if (UWorld* World = GetWorld())
	{
		CombatController = (AQECombatController*)UGameplayStatics::GetActorOfClass(World, AQECombatController::StaticClass());
	}
	if (CombatController == nullptr)
	{
		return;
	}
	if (UWorld* World = GetWorld())
	{
		for (AQEUnit* OldUnit : OldTeamOneArr)
		{
			OldUnit->Destroy();
		}
		for (AQEUnit* OldUnit : OldTeamTwoArr)
		{
			OldUnit->Destroy();
		}
		if (TeamOne)
		{
			TeamOne->Destroy();
		}
		if (TeamTwo)
		{
			TeamTwo->Destroy();
		}
		TeamOne = World->SpawnActor<AQETeam>(AQETeam::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
		for (AQEUnit* Unit : TeamOneArr)
		{
			TeamOne->RegisterUnit(Unit);
		}
		TeamTwo = World->SpawnActor<AQETeam>(AQETeam::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
		for (AQEUnit* Unit : TeamTwoArr)
		{
			TeamTwo->RegisterUnit(Unit);
		}
		CombatController->SetTeams(TeamOne, TeamTwo);

		OldTeamOneArr = TeamOneArr;
		OldTeamTwoArr = TeamTwoArr;
		TeamOneArr = TArray<AQEUnit*>();
		TeamTwoArr = TArray<AQEUnit*>();
		CombatController->StartCombat();

		ACombatMap* Map = CombatController->GetCombatMap();
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Map->GetName());
		int i = 0;
		int j = 0;
		for (AQEUnit* TeamOneUnit : TeamOne->GetUnits())
		{
			Map->MoveUnit(TeamOneUnit, FCombatLocator(0, j, i));
			i = (i + 1) % 3;
			if (i == 0)
			{
				j++;
			}
		}
		i = 0;
		j = 0;
		for (AQEUnit* TeamTwoUnit : TeamTwo->GetUnits())
		{
			Map->MoveUnit(TeamTwoUnit, FCombatLocator(1, j, i));
			i = (i + 1) % 3;
			if (i == 0)
			{
				j++;
			}
		}
	}
}

void ACombatDebugger::InitializeCombatController()
{
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "QEAttributes.h"
#include "QEAttribute.h"

UQEAttributes::UQEAttributes()
{
	auto MakeAttr = [this](FName Name, float Value) -> UQEAttribute* {
		UQEAttribute* Attr = CreateDefaultSubobject<UQEAttribute>(Name);
		Attr->SetBaseValue(Value);
		Attr->SetCurrentValue(Value);
		return Attr;
	};
	Health = MakeAttr(FName("Health"), 100.f);
	Mana = MakeAttr(FName("Mana"), 100.f);
	MaxHealth = MakeAttr(FName("MaxHealth"), 100.f);
	MaxMana = MakeAttr(FName("MaxMana"), 100.f);
	DefenseBase = MakeAttr(FName("DefenseBase"), 1.f);
	DefenseFire = MakeAttr(FName("DefenseFire"), 1.f);
	StrengthBase = MakeAttr(FName("StrengthBase"), 1.f);
	StrengthFire = MakeAttr(FName("StrengthFire"), 1.f);
}

UQEAttribute* UQEAttributes::GetHealth() const
{
	return Health;
}

UQEAttribute* UQEAttributes::GetMana() const
{
	return Mana;
}

UQEAttribute* UQEAttributes::GetMaxHealth() const
{
	return MaxHealth;
}

UQEAttribute* UQEAttributes::GetMaxMana() const
{
	return MaxMana;
}

UQEAttribute* UQEAttributes::GetDefenseBase() const
{
	return DefenseBase;
}

UQEAttribute* UQEAttributes::GetDefenseFire() const
{
	return DefenseFire;
}

UQEAttribute* UQEAttributes::GetStrengthBase() const
{
	return StrengthBase;
}

UQEAttribute* UQEAttributes::GetStrengthFire() const
{
	return StrengthFire;
}


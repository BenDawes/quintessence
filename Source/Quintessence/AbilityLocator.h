// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityLocator.generated.h"

UENUM(BlueprintType)
enum class EAbilityLocator : uint8
{
	Left,
	Top,
	Right,
	Bottom,
	None
};

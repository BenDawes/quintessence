// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEUnit.h"
#include "QEUnitAbility.h"
#include "Ability_Bite.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UAbility_Bite : public UQEUnitAbility
{
	GENERATED_BODY()
public:
	UAbility_Bite();

	void ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap) override;
};

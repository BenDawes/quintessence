// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityLocator.h"
#include "FCombatCell.h"
#include "FCombatLocator.h"
#include "CombatTargeter.generated.h"

class ACombatMap;

/**
 *
 */
UCLASS()
class QUINTESSENCE_API ACombatTargeter : public AActor
{
	GENERATED_BODY()
public:
	ACombatTargeter();

	UPROPERTY(BlueprintReadWrite)
	FCombatLocator CenterLocator = FCombatLocator();

	int ConvertColumnBetweenTeams(ACombatMap* CombatMap, int OldColumn, int FromTeamIndex, int ToTeamIndex);

	virtual void Move(EAbilityLocator Direction, ACombatMap* CombatMap);
	void PostMove(ACombatMap* CombatMap);
	virtual TArray<FCombatCell> GetAllCells(ACombatMap* CombatMap);

	UPROPERTY()
	UStaticMesh* TargeterMesh;

	FCombatLocator WrapToMap(FCombatLocator InLocator, ACombatMap* CombatMap);
	FCombatLocator ClampLocator(FCombatLocator InLocator, ACombatMap* CombatMap);
	FCombatLocator UpdateLocatorUnchecked(FCombatLocator InLocator, ACombatMap* CombatMap, EAbilityLocator Direction);
};

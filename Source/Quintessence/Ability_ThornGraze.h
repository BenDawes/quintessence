// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEUnitAbility.h"
#include "Ability_ThornGraze.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UAbility_ThornGraze : public UQEUnitAbility
{
	GENERATED_BODY()
public:
	UAbility_ThornGraze();

	void ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap) override;
	
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "CS_TargetingAbility.h"
#include "QECombatStateType.h"
#include "QECombatStateMachine.h"

UCS_TargetingAbility::UCS_TargetingAbility()
{
	StateType = EQECombatStateType::TargetingAbility;
}

void UCS_TargetingAbility::OnEnter(UQECombatStateMachine* Combat)
{
	Super::OnEnter(Combat);
	UE_LOG(LogTemp, Log, TEXT("Resolving ability entered"));
}

void UCS_TargetingAbility::OnExit(UQECombatStateMachine* Combat)
{
	Super::OnExit(Combat);
	UE_LOG(LogTemp, Log, TEXT("Resolving ability exited"));
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "CS_AbilityResolving.h"
#include "QECombatStateType.h"
#include "QECombatStateMachine.h"

UCS_AbilityResolving::UCS_AbilityResolving()
{
	StateType = EQECombatStateType::AbilityResolving;
}

void UCS_AbilityResolving::OnEnter(UQECombatStateMachine* Combat)
{
	Super::OnEnter(Combat);
	StoredCombat = Combat;
	UE_LOG(LogTemp, Log, TEXT("Resolving ability entered"));
	GetWorld()->GetTimerManager().SetTimer(FinishedResolvingHandle, this, &UCS_AbilityResolving::OnFinishedResolving, 2, false);
}

void UCS_AbilityResolving::OnExit(UQECombatStateMachine* Combat)
{
	Super::OnExit(Combat);
	UE_LOG(LogTemp, Log, TEXT("Resolving ability exited"));
}

void UCS_AbilityResolving::OnFinishedResolving()
{
	StoredCombat->Notify(ECombatNotification::AbilityFinished);
}
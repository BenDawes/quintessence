// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "AbilityLocator.h"
#include "QECombatStateType.h"
#include "QETeam.h"
#include "QEUnit.h"
#include "QEUnitAbility.h"
#include "MoveSelectorCPP.generated.h"

class AQECombatController;

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UMoveSelectorCPP : public UUserWidget
{
	GENERATED_BODY()
private:
	UQEUnitAbility* GetAbility(EAbilityLocator Locator);
public:
	UPROPERTY(BlueprintReadOnly)
	AQEUnit* Unit;

	UPROPERTY(BlueprintReadOnly)
	AQETeam* OwningTeam;

	void SetOwningTeam(AQETeam* Team);

	UPROPERTY(BlueprintReadOnly)
	AQECombatController* CombatController;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EAbilityLocator SelectedAction = EAbilityLocator::None;

	UFUNCTION(BlueprintCallable)
	void SelectUnit();

	UFUNCTION(BlueprintCallable)
	void BeginTargetingAbility(EAbilityLocator Locator);

	UFUNCTION(BlueprintCallable)
	void EnactAbility(EAbilityLocator Locator);

	UFUNCTION(BlueprintCallable)
	void CancelTargetingAbility();

	UFUNCTION(BlueprintPure)
	EQECombatStateType GetCombatStateType();

	UFUNCTION(BlueprintPure)
	AQETeam* GetActiveTeam();

	UFUNCTION(BlueprintPure)
	FString GetLargeText();

	UFUNCTION(BlueprintPure)
	AQEUnit* GetTargetedUnitForSelection();

	UFUNCTION()
	void SetCombatController(AQECombatController* NewCombatController);

	void BindToExternalInputComponent(UInputComponent* ExternalInputComponent);

	template<EAbilityLocator Locator, bool bWasAlt> void InputPressed();
	void InputPressed(EAbilityLocator Locator, bool bWasAlt);

	template<EAbilityLocator Locator, bool bWasAlt> void InputReleased();
	void InputReleased(EAbilityLocator Locator, bool bWasAlt);

	UFUNCTION(BlueprintCallable)
	void InputConfirmed(EAbilityLocator Locator, bool bWasAlt);
	
	UFUNCTION(BlueprintCallable)
	void InputCancelled();
};

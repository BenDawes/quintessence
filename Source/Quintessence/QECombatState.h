// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QECombatStateType.h"
#include "QECombatState.generated.h"

class UQECombatStateMachine;

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UQECombatState : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly)
	EQECombatStateType StateType;

	UQECombatState();

	virtual void OnEnter(UQECombatStateMachine* Combat);
	virtual void OnExit(UQECombatStateMachine* Combat);
};


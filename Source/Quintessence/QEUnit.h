// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributePercentageBarCPP.h"
#include "Components/WidgetComponent.h"
#include "FCombatLocator.h"
#include "QEUnitAbility.h"
#include "QEAttributes.h"
#include "QEUnit.generated.h"

UCLASS()
class QUINTESSENCE_API AQEUnit : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AQEUnit();

protected:

	UPROPERTY()
	UQEUnitAbilities* Abilities;


	void AssignSkeleton(const TCHAR* PathToSkeletalMesh);
public:	

	virtual void BeginPlay() override;


	UPROPERTY()
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY()
	UWidgetComponent* HealthBarWidget;

	UPROPERTY()
	UAttributePercentageBarCPP* HealthBar;

	UFUNCTION()
	void GainAbility(UQEUnitAbility* Ability);

	UFUNCTION(BlueprintCallable)
	UQEUnitAbilities* GetAbilities() const;

	UPROPERTY(BlueprintReadOnly)
	UQEAttributes* Attributes;

	UPROPERTY()
	FCombatLocator CombatLocator;

	UPROPERTY(EditAnywhere, Category = "UI")
	TSubclassOf<class UUserWidget> PercentageBarClass;
};

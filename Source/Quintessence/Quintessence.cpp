// Copyright Epic Games, Inc. All Rights Reserved.

#include "Quintessence.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Quintessence, "Quintessence" );
 
// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatTargeter.h"
#include "CoreMinimal.h"
#include "AbilityLocator.h"
#include "FCombatCell.h"
#include "CombatMap.h"

ACombatTargeter::ACombatTargeter()
{
    RootComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
    static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
    if (MeshAsset.Succeeded())
    {
        TargeterMesh = MeshAsset.Object;
    }
    UStaticMeshComponent* StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Static Mesh"));
    StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
    if (TargeterMesh)
    {
        StaticMesh->SetStaticMesh(TargeterMesh);
    }
    CenterLocator = FCombatLocator();
}

int ACombatTargeter::ConvertColumnBetweenTeams(ACombatMap* CombatMap, int OldColumn, int FromTeamIndex, int ToTeamIndex)
{
    int OldWidth = CombatMap->GetTeamWidth(FromTeamIndex);
    int NewWidth = CombatMap->GetTeamWidth(ToTeamIndex);
    if (OldWidth >= NewWidth)
    {
        return OldColumn >= NewWidth ? NewWidth - 1 : OldColumn;
    }
    float OldHalfWidth = ((float)OldWidth) / 2.f;
    float NewHalfWidth = ((float)NewWidth) / 2.f;

    float OffsetFromCenter = OldColumn - OldHalfWidth;
    int NewColumn = FMath::FloorToInt(NewHalfWidth + OffsetFromCenter);
    return NewColumn;
}

void ACombatTargeter::Move(EAbilityLocator Direction, ACombatMap* CombatMap)
{
    FCombatLocator NewLocator = UpdateLocatorUnchecked(CenterLocator, CombatMap, Direction);
    CenterLocator = WrapToMap(NewLocator, CombatMap);
    PostMove(CombatMap);
}

FCombatLocator ACombatTargeter::UpdateLocatorUnchecked(FCombatLocator InLocator, ACombatMap* CombatMap, EAbilityLocator Direction)
{
    int NewRowIndex = InLocator.RowIndex;
    int NewColumnIndex = InLocator.ColumnIndex;
    int NewTeamIndex = InLocator.TeamIndex;
    switch (Direction)
    {
        case EAbilityLocator::Left:
        {
            NewRowIndex -= 1;
            break;
        }
        case EAbilityLocator::Right:
        {
            NewRowIndex += 1;
            break;
        }
        case EAbilityLocator::Top:
        {
            NewColumnIndex += 1;
            break;
        }
        case EAbilityLocator::Bottom:
        {
            NewColumnIndex -= 1;
            break;
        }
        default:
            return InLocator;
    }
    return FCombatLocator(NewTeamIndex, NewRowIndex, NewColumnIndex);
}

FCombatLocator ACombatTargeter::WrapToMap(FCombatLocator InLocator, ACombatMap* CombatMap)
{
    int MaxWidth = CombatMap->GetTeamWidth(InLocator.TeamIndex);
    int MaxHeight = CombatMap->GetTeamHeight(InLocator.TeamIndex);
    int NewRowIndex = InLocator.RowIndex;
    int NewColumnIndex = InLocator.ColumnIndex;
    int NewTeamIndex = InLocator.TeamIndex;

    if (NewRowIndex < 0 || NewRowIndex >= MaxHeight)
    {
        NewColumnIndex = ConvertColumnBetweenTeams(CombatMap, NewColumnIndex, NewTeamIndex, 1 - NewTeamIndex);
        NewRowIndex = CombatMap->GetTeamHeight(1 - NewTeamIndex) - 1;
        NewTeamIndex = 1 - NewTeamIndex;
    }
    if (NewColumnIndex < 0)
    {
        NewColumnIndex = CombatMap->GetTeamWidth(NewTeamIndex) - 1;
    }
    if (NewColumnIndex >= CombatMap->GetTeamWidth(NewTeamIndex))
    {
        NewColumnIndex = 0;
    }

    return FCombatLocator(NewTeamIndex, NewRowIndex, NewColumnIndex);
}

FCombatLocator ACombatTargeter::ClampLocator(FCombatLocator InLocator, ACombatMap* CombatMap)
{
    int MaxWidth = CombatMap->GetTeamWidth(InLocator.TeamIndex);
    int MaxHeight = CombatMap->GetTeamHeight(InLocator.TeamIndex);
    int NewRowIndex = InLocator.RowIndex;
    int NewColumnIndex = InLocator.ColumnIndex;
    int NewTeamIndex = InLocator.TeamIndex;
    if (NewRowIndex < 0)
    {
        NewRowIndex = MaxHeight - 1;
    }
    if (NewRowIndex >= MaxHeight)
    {
        NewRowIndex = 0;
    }
    if (NewColumnIndex < 0)
    {
        NewColumnIndex = MaxWidth - 1;
    }
    if (NewColumnIndex >= MaxWidth)
    {
        NewColumnIndex = 0;
    }
    return FCombatLocator(NewTeamIndex, NewRowIndex, NewColumnIndex);
}

void ACombatTargeter::PostMove(ACombatMap* CombatMap)
{
    FVector NewLocation = CombatMap->GridLocationToWorldLocationFromLocator(CenterLocator);
    NewLocation.Z += 200;
    SetActorLocation(NewLocation);
}

TArray<FCombatCell> ACombatTargeter::GetAllCells(ACombatMap* CombatMap)
{
    return TArray<FCombatCell>();
}

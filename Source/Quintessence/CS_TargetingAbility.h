// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QECombatState.h"
#include "QECombatStateMachine.h"
#include "CS_TargetingAbility.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UCS_TargetingAbility : public UQECombatState
{
	GENERATED_BODY()
public:
	UCS_TargetingAbility();

	virtual void OnEnter(UQECombatStateMachine* Combat) override;
	virtual void OnExit(UQECombatStateMachine* Combat) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "QECombatStateType.generated.h"

UENUM(BlueprintType)
enum class EQECombatStateType : uint8
{
	ChoosingUnit 		UMETA(DisplayName = "ChoosingUnit"),
	ChoosingAbility		UMETA(DisplayName= "ChoosingAbility"),
	TargetingAbility	UMETA(DisplayName = "TargetingAbility"),
	AbilityResolving	UMETA(DisplayName = "AbilityResolving")
};

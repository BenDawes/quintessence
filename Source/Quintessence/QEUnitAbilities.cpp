// Fill out your copyright notice in the Description page of Project Settings.

#include "QEUnitAbilities.h"
#include "QEUnitAbility.h"
#include "AbilityLocator.h"

UQEUnitAbilities::UQEUnitAbilities()
{
}

UQEUnitAbility* UQEUnitAbilities::GetAbilityByLocator(EAbilityLocator Locator)
{
    int Index = GetIndexFromLocator(Locator);
    if (Index != -1 && Abilities.Num() > Index)
    {
        return Abilities[Index];
    }
    return nullptr;
}

int UQEUnitAbilities::GetIndexFromLocator(EAbilityLocator Locator)
{
    switch (Locator)
    {
    case EAbilityLocator::Left:
        return 0;
    case EAbilityLocator::Top:
        return 1;
    case EAbilityLocator::Right:
        return 2;
    case EAbilityLocator::Bottom:
        return 3;
    default:
        return -1;
    }
}

void UQEUnitAbilities::GainAbility(UQEUnitAbility* Ability)
{
    Abilities.Add(Ability);
}

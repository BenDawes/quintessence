#pragma once

#include "CoreMinimal.h"
#include "FCombatLocator.generated.h"

USTRUCT(BlueprintType)
struct FCombatLocator
{
    GENERATED_BODY()

    FCombatLocator() {}
    FCombatLocator(int _TeamIndex, int _RowIndex, int _ColumnIndex) { TeamIndex = _TeamIndex; RowIndex = _RowIndex; ColumnIndex = _ColumnIndex; }

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Locator")
    int TeamIndex = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Locator")
    int RowIndex = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Locator")
    int ColumnIndex = 0;

};
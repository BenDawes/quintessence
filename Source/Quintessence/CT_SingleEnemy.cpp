// Fill out your copyright notice in the Description page of Project Settings.

#include "CT_SingleEnemy.h"
#include "CombatMap.h"
#include "FCombatCell.h"

ACT_SingleEnemy::ACT_SingleEnemy()
{
    CenterLocator = FCombatLocator(1, 0, 0);
}

TArray<FCombatCell> ACT_SingleEnemy::GetAllCells(ACombatMap* CombatMap)
{
    TArray<FCombatCell> Result = TArray<FCombatCell>();
    Result.Add(*CombatMap->GetCell(CenterLocator));
    return Result;
}

void ACT_SingleEnemy::Move(EAbilityLocator Direction, ACombatMap* CombatMap)
{
    FCombatLocator NewLocator = UpdateLocatorUnchecked(CenterLocator, CombatMap, Direction);
    CenterLocator = ClampLocator(NewLocator, CombatMap);
    PostMove(CombatMap);
}

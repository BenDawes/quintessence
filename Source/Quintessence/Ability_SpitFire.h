// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEUnitAbility.h"
#include "Ability_SpitFire.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UAbility_SpitFire : public UQEUnitAbility
{
	GENERATED_BODY()
public:
	UAbility_SpitFire();

	void ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap) override;
};

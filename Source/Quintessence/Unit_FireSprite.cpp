// Fill out your copyright notice in the Description page of Project Settings.


#include "Unit_FireSprite.h"
#include "Ability_SpitFire.h"
#include "Ability_Bite.h"

AUnit_FireSprite::AUnit_FireSprite()
{
	GainAbility(CreateDefaultSubobject<UAbility_Bite>(FName("Ability_Bite")));
	GainAbility(CreateDefaultSubobject<UAbility_SpitFire>(FName("Ability_SpitFire")));
	AssignSkeleton(TEXT("SkeletalMesh'/Game/AnimalVarietyPack/Fox/Meshes/SK_Fox.SK_Fox'"));
}
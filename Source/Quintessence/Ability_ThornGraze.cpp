// Fill out your copyright notice in the Description page of Project Settings.


#include "Ability_ThornGraze.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEAttribute.h"
#include "QEUnit.h"

UAbility_ThornGraze::UAbility_ThornGraze()
{
	AbilityCost = 10.0f;
	AbilityName = FString("Thorn Graze");
	AbilityDescription = FString("Grazes a single target, dealing damage");
	ConfigureSingleEnemyTargeter();
}

void UAbility_ThornGraze::ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap)
{
	Super::ApplyToCombat(SourceUnit, CombatMap);
	TArray<FCombatCell> TargetCells = GetAllTargetedCells(CombatMap);
	for (FCombatCell TargetCell : TargetCells)
	{
		for (AQEUnit* Target : TargetCell.Units)
		{
			UQEAttribute* Health = Target->Attributes->GetHealth();
			Health->SetCurrentValue(Health->GetCurrentValue() - 10.f);
		}
	}
}

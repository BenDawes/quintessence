// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatDebugger.h"
#include "ControlType.h"
#include "GameFramework/PlayerController.h"
#include "QECombatController.h"
#include "QEPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API AQEPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	UPROPERTY()
	AQECombatController* CombatController;
	UPROPERTY()
	ACombatDebugger* CombatDebugger;
	
	EControlType ControlType;
public:
	AQEPlayerController();

	virtual void BeginPlay() override;

	virtual void AcknowledgePossession(class APawn* NewPawn) override;

	UFUNCTION()
	void OnControlTypeChange(EControlType NewType, EControlType OldType);
};

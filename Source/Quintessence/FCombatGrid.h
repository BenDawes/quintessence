// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FCombatRow.h"
#include "FCombatGrid.generated.h"

USTRUCT(BlueprintType)
struct QUINTESSENCE_API FCombatGrid
{
    GENERATED_BODY()

    UPROPERTY()
    TMap<int, FCombatRow> Rows;

    FCombatGrid() {}

    FCombatGrid(int Height, int Width)
    {
        for (int i = 0; i < Height; i++)
        {
            Rows.Emplace(i, FCombatRow(Width));
        }
    }
};
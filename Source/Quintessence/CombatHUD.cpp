// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatHUD.h"
#include "Blueprint/UserWidget.h"
#include "QECombatController.h"
#include "QECombatStateMachine.h"
#include "MoveSelectorCPP.h"

ACombatHUD::ACombatHUD()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> AbilitiesWidgetClassFinder(TEXT("/Game/ThirdPersonCPP/CombatUI/MoveSelector"));
	AbilitiesWidgetClass = AbilitiesWidgetClassFinder.Class;
}

void ACombatHUD::BeginPlay()
{
	Super::BeginPlay();
	if (AbilitiesWidgetClass != nullptr)
	{
		CurrentAbilitiesWidget = CreateWidget<UMoveSelectorCPP>(GetWorld(), AbilitiesWidgetClass);

		if (CurrentAbilitiesWidget)
		{
			CurrentAbilitiesWidget->AddToViewport();
			if (CombatState)
			{
				OnUnitSelected(CombatState->GetSelectedUnit(), nullptr);
			}
		}
	}

}

void ACombatHUD::DrawHUD()
{
	Super::DrawHUD();
}

void ACombatHUD::SetCombatStateMachine(UQECombatStateMachine* NewState)
{
	CombatState = NewState;
	CombatState->OnUnitSelected.AddDynamic(this, &ACombatHUD::OnUnitSelected);
	OnUnitSelected(CombatState->GetSelectedUnit(), nullptr);
}

void ACombatHUD::SetCombatController(AQECombatController* NewCombatController)
{
	CombatController = NewCombatController;
	if (CurrentAbilitiesWidget)
	{
		CurrentAbilitiesWidget->SetCombatController(CombatController);
	}
}

void ACombatHUD::OnUnitSelected(AQEUnit* NewUnit, AQEUnit* OldUnit)
{
	if (CurrentAbilitiesWidget)
	{
		CurrentAbilitiesWidget->Unit = NewUnit;
	}
}

void ACombatHUD::SetOwningTeam(AQETeam* Team)
{
	if (CurrentAbilitiesWidget)
	{
		CurrentAbilitiesWidget->SetOwningTeam(Team);
	}
}

void ACombatHUD::BindToExternalInputComponent(UInputComponent* ExternalInputComponent)
{
	if (CurrentAbilitiesWidget)
	{
		CurrentAbilitiesWidget->BindToExternalInputComponent(ExternalInputComponent);
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "AttributePercentageBarCPP.h"

UQEAttribute* UAttributePercentageBarCPP::GetAttribute()
{
	return Attribute;
}

void UAttributePercentageBarCPP::SetAttribute(UQEAttribute* NewAttribute)
{
	Attribute = NewAttribute;
}

UQEAttribute* UAttributePercentageBarCPP::GetMaxAttribute()
{
	return MaxAttribute;
}

void UAttributePercentageBarCPP::SetMaxAttribute(UQEAttribute* NewAttribute)
{
	MaxAttribute = NewAttribute;
}

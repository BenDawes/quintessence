// Fill out your copyright notice in the Description page of Project Settings.


#include "CS_ChoosingUnit.h"
#include "QECombatStateType.h"
#include "QECombatStateMachine.h"

UCS_ChoosingUnit::UCS_ChoosingUnit()
{
	StateType = EQECombatStateType::ChoosingUnit;
}

void UCS_ChoosingUnit::OnEnter(UQECombatStateMachine* Combat)
{
	Super::OnEnter(Combat);
	UE_LOG(LogTemp, Log, TEXT("Choosing unit entered"));
}

void UCS_ChoosingUnit::OnExit(UQECombatStateMachine* Combat)
{
	Super::OnExit(Combat);
	UE_LOG(LogTemp, Log, TEXT("Choosing unit exited"));
}
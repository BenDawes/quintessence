// Fill out your copyright notice in the Description page of Project Settings.


#include "QEUnit.h"
#include "AttributePercentageBarCPP.h"
#include "FCombatLocator.h"
#include "QEUnitAbilities.h"
#include "QEAttributes.h"
#include "Components/WidgetComponent.h"

// Sets default values
AQEUnit::AQEUnit()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	Attributes = CreateDefaultSubobject<UQEAttributes>(FName("Attributes"));
	Abilities = CreateDefaultSubobject<UQEUnitAbilities>(FName("Abilities"));
	static ConstructorHelpers::FClassFinder<UUserWidget> PercentageBarCF(TEXT("/Game/ThirdPersonCPP/CombatUI/AttributePercentageBar"));
	HealthBarWidget = CreateDefaultSubobject<UWidgetComponent>("HealthBar");
	HealthBarWidget->AttachTo(RootComponent);
	HealthBarWidget->SetWidgetSpace(EWidgetSpace::Screen);
	HealthBarWidget->SetRelativeLocation(FVector(0, 0, 75));
	HealthBarWidget->SetDrawSize(FVector2D(100, 40));
	if (PercentageBarCF.Succeeded())
	{
		HealthBarWidget->SetWidgetClass(PercentageBarCF.Class);
	}
	CombatLocator = FCombatLocator();
}

void AQEUnit::BeginPlay()
{
	Super::BeginPlay();
	HealthBar = Cast<UAttributePercentageBarCPP>(HealthBarWidget->GetWidget());
	HealthBar->SetAttribute(Attributes->GetHealth());
	HealthBar->SetMaxAttribute(Attributes->GetMaxHealth());
}

void AQEUnit::AssignSkeleton(const TCHAR* PathToSkeletalMesh)
{
	if (!SkeletalMesh)
	{
		SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName("SkeletalMesh"));
	}
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> MeshAsset(PathToSkeletalMesh);
	if (SkeletalMesh && MeshAsset.Succeeded())
	{
		SkeletalMesh->AttachTo(RootComponent);
		SkeletalMesh->SetSkeletalMesh(MeshAsset.Object);
	}
}

void AQEUnit::GainAbility(UQEUnitAbility* Ability)
{
	Abilities->GainAbility(Ability);
}

UQEUnitAbilities* AQEUnit::GetAbilities() const
{
	return Abilities;
}


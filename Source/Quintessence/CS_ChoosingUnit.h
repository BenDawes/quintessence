// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QECombatState.h"
#include "QECombatStateMachine.h"
#include "CS_ChoosingUnit.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UCS_ChoosingUnit : public UQECombatState
{
	GENERATED_BODY()
public:
	UCS_ChoosingUnit();

	virtual void OnEnter(UQECombatStateMachine* Combat) override;
	virtual void OnExit(UQECombatStateMachine* Combat) override;
};

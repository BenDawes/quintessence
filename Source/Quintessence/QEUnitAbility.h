// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityLocator.h"
#include "FCombatLocator.h"
#include "CombatTargeter.h"
#include "QEUnitAbility.generated.h"

class AQEUnit;
class ACombatMap;

/**
 * 
 */
UCLASS(Blueprintable)
class QUINTESSENCE_API UQEUnitAbility : public UObject
{
    GENERATED_BODY()

protected:
    void ConfigureSingleEnemyTargeter();
    void ConfigureSingleAllyTargeter();

public:
    UQEUnitAbility();

    UPROPERTY(BlueprintReadOnly)
        FString AbilityName;

    UPROPERTY(BlueprintReadOnly)
        FString AbilityDescription;

    UPROPERTY(BlueprintReadOnly)
        float AbilityCost;

    UFUNCTION(BlueprintCallable)
        virtual void ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap);

    UPROPERTY(BlueprintReadOnly)
        UChildActorComponent* TargeterComp;

    UFUNCTION()
        void MoveTargeter(EAbilityLocator Direction, ACombatMap* CombatMap);

    UFUNCTION()
        void UpdateTargeter(ACombatMap* CombatMap);

    UFUNCTION()
        TArray<FCombatCell> GetAllTargetedCells(ACombatMap* CombatMap);

    UFUNCTION()
        void SetTargeterVisibility(bool bIsVisible);
};
// Fill out your copyright notice in the Description page of Project Settings.


#include "Ability_Bite.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEAttribute.h"
#include "QEUnit.h"


UAbility_Bite::UAbility_Bite()
{
	AbilityCost = 10.0f;
	AbilityName = FString("Bite");
	AbilityDescription = FString("Damages a single target");
	ConfigureSingleEnemyTargeter();
}

void UAbility_Bite::ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap)
{
	Super::ApplyToCombat(SourceUnit, CombatMap);
	TArray<FCombatCell> TargetCells = GetAllTargetedCells(CombatMap);
	for (FCombatCell TargetCell : TargetCells)
	{
		for (AQEUnit* Target : TargetCell.Units)
		{
			UQEAttribute* Health = Target->Attributes->GetHealth();
			Health->SetCurrentValue(Health->GetCurrentValue() - 50.f);
		}
	}
}
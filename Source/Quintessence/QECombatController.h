// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityLocator.h"
#include "CombatHUD.h"
#include "CombatTargeter.h"
#include "QECombatStateMachine.h"
#include "QECombatState.h"
#include "QEUnit.h"
#include "QEUnitAbility.h"
#include "QECombatController.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API AQECombatController : public AActor
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	UQECombatStateMachine* Combat;
	
	UPROPERTY()
	ACombatMap* CombatMap;

	UPROPERTY()
	ACombatTargeter* CombatTargeter;

public:
	AQECombatController();

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void StartCombat();

	UFUNCTION(BlueprintCallable)
	void SelectUnit(AQEUnit* Unit);

	UFUNCTION(BlueprintCallable)
	void BeginTargetingAbility(AQEUnit* SourceUnit, EAbilityLocator Locator);

	UFUNCTION(BlueprintCallable)
	void ConfirmAbility(AQEUnit* SourceUnit, EAbilityLocator Locator);

	UFUNCTION(BlueprintCallable)
	void StopTargeting();
	
	UFUNCTION(BlueprintCallable)
	void SetTeams(AQETeam* TeamOne, AQETeam* TeamTwo);

	UFUNCTION()
	void OnStateChange(UQECombatState* NewState, UQECombatState* OldState);

	UFUNCTION()
	void OnActiveTeamChange(AQETeam* NewActiveTeam, AQETeam* NewInactiveTeam);

	void SetHUD(ACombatHUD* NewHUD);

	UFUNCTION()
	ACombatMap* GetCombatMap();

	UFUNCTION()
	UQECombatStateMachine* GetCombat();

	UFUNCTION()
	void MoveTargeter(EAbilityLocator Direction);

	UPROPERTY(BlueprintReadOnly)
	UChildActorComponent* TargeterComp;

	UFUNCTION()
	ACombatTargeter* GetTargeter();
};

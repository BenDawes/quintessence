// Fill out your copyright notice in the Description page of Project Settings.


#include "QEGameState.h"

EControlType AQEGameState::GetControlType()
{
	return ControlType;
}

void AQEGameState::SetControlType(EControlType NewControlType)
{
	EControlType OldControlType = ControlType;
	ControlType = NewControlType;
	OnControlTypeChange.Broadcast(NewControlType, OldControlType);
}

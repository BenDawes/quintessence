// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "QEAttribute.h"
#include "QEAttributes.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UQEAttributes : public UObject
{
	GENERATED_BODY()
public:
	UQEAttributes();
protected:
	UPROPERTY()
		UQEAttribute* Health;

	UPROPERTY()
		UQEAttribute* Mana;

	UPROPERTY()
		UQEAttribute* MaxHealth;

	UPROPERTY()
		UQEAttribute* MaxMana;

	UPROPERTY()
		UQEAttribute* DefenseBase;

	UPROPERTY()
		UQEAttribute* DefenseFire;
	
	UPROPERTY()
		UQEAttribute* StrengthBase;

	UPROPERTY()
		UQEAttribute* StrengthFire;


public:
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetHealth() const;
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetMana() const;
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetMaxHealth() const;
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetMaxMana() const;
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetDefenseBase() const;
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetDefenseFire() const;
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetStrengthBase() const;
	UFUNCTION(BlueprintCallable)
		UQEAttribute* GetStrengthFire() const;

};

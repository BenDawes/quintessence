// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "QEAttribute.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UQEAttribute : public UObject
{
	GENERATED_BODY()
protected:
	UPROPERTY()
		float BaseValue;

	UPROPERTY()
		float CurrentValue;

public:
	UFUNCTION(BlueprintCallable)
		void SetBaseValue(float NewBaseValue);

	UFUNCTION(BlueprintCallable)
		void SetCurrentValue(float NewCurrentValue);

	UFUNCTION(BlueprintCallable)
		void SetValues(float NewCurrentValue, float NewBaseValue);

	UFUNCTION(BlueprintCallable)
		float GetBaseValue() const;

	UFUNCTION(BlueprintCallable)
		float GetCurrentValue() const;
};

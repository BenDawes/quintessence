// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QECombatStates.h"
#include "QECombatState.h"
#include "QEUnit.h"
#include "QETeam.h"
#include "CombatNotification.h"
#include "QECombatStateMachine.generated.h"


UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStateChangeDelegate, UQECombatState*, NewState, UQECombatState*, OldState);


UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUnitSelectedDelegate, AQEUnit*, NewUnit, AQEUnit*, OldUnit);


UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnActiveTeamChangeDelegate, AQETeam*, NewActiveTeam, AQETeam*, NewInactiveTeam);

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UQECombatStateMachine : public UObject
{
	GENERATED_BODY()
public:
	UQECombatStateMachine();
protected:
	UPROPERTY()
	UQECombatStates* States;
	UPROPERTY()
	UQECombatState* CurrentState;

	UPROPERTY()
	AQEUnit* SelectedUnit;

	UPROPERTY()
	AQETeam* ActiveTeam;

	UPROPERTY()
	AQETeam* InactiveTeam;

	UPROPERTY()
	AQETeam* TeamOne;

	UPROPERTY()
	AQETeam* TeamTwo;

	UPROPERTY()
	UQEUnitAbility* CurrentlyActiveAbility;

public:
	void SelectUnit(AQEUnit* Unit);
	void ChangeActiveTeam();

	UFUNCTION(BlueprintPure)
	AQETeam* GetActiveTeam();

	UFUNCTION()
	void SetActiveTeam(AQETeam* NewActiveTeam);
	
	UFUNCTION()
	void SetTeams(AQETeam* NewTeamOne, AQETeam* NewTeamTwo);

	UFUNCTION()
	UQEUnitAbility* GetCurrentlyActiveAbility();

	UFUNCTION()
	void SetCurrentlyActiveAbility(UQEUnitAbility* NewActiveAbility);

	AQEUnit* GetSelectedUnit();
	void ForceState(UQECombatState* NewState);

	void Start();

	void Notify(ECombatNotification Notification);

	UPROPERTY()
	FOnStateChangeDelegate OnStateChange;
	
	UPROPERTY(BlueprintAssignable)
	FOnUnitSelectedDelegate OnUnitSelected;

	UPROPERTY(BlueprintAssignable)
	FOnActiveTeamChangeDelegate OnActiveTeamChange;

	UFUNCTION(BlueprintPure)
	AQETeam* GetTeamOne();

	UFUNCTION(BlueprintPure)
	AQETeam* GetTeamTwo();

	UFUNCTION(BlueprintPure)
	UQECombatState* GetCurrentState();
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "CS_ChoosingAbility.h"
#include "QECombatStateType.h"
#include "QECombatStateMachine.h"

UCS_ChoosingAbility::UCS_ChoosingAbility()
{
	StateType = EQECombatStateType::ChoosingAbility;
}

void UCS_ChoosingAbility::OnEnter(UQECombatStateMachine* Combat)
{
	Super::OnEnter(Combat);
	UE_LOG(LogTemp, Log, TEXT("Choosing ability entered"));
}

void UCS_ChoosingAbility::OnExit(UQECombatStateMachine* Combat)
{
	Super::OnExit(Combat);
	UE_LOG(LogTemp, Log, TEXT("Choosing ability exited"));
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QECombatState.h"
#include "QECombatStateMachine.h"
#include "CS_ChoosingAbility.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UCS_ChoosingAbility : public UQECombatState
{
	GENERATED_BODY()
public:
	UCS_ChoosingAbility();

	virtual void OnEnter(UQECombatStateMachine* Combat) override;
	virtual void OnExit(UQECombatStateMachine* Combat) override;
};

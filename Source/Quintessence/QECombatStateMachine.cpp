// Fill out your copyright notice in the Description page of Project Settings.


#include "QECombatStateMachine.h"
#include "QECombatStates.h"
#include "QECombatState.h"
#include "QETeam.h"
#include "QEUnit.h"
#include "CombatNotification.h"

UQECombatStateMachine::UQECombatStateMachine()
{
	States = CreateDefaultSubobject<UQECombatStates>(FName("States"));
	CurrentState = States->ChoosingAbility;
}

void UQECombatStateMachine::ForceState(UQECombatState* NewState)
{
	CurrentState = NewState;
}

void UQECombatStateMachine::Start()
{
	CurrentState->OnEnter(this);
}

void UQECombatStateMachine::Notify(ECombatNotification Notification)
{
	UQECombatState* StartState = CurrentState;
	switch (Notification) 
	{
		case ECombatNotification::UnitSelected:
		{
			CurrentState = States->ChoosingAbility;
			break;
		}
		case ECombatNotification::TargetingStarted:
		{
			if (CurrentlyActiveAbility)
			{
				CurrentlyActiveAbility->SetTargeterVisibility(true);
			}
			CurrentState = States->TargetingAbility;
			break;
		}
		case ECombatNotification::AbilityConfirmed:
		{
			CurrentState = States->AbilityResolving;
			break;
		}
		case ECombatNotification::ChoosingAbilityCanceled:
		{
			CurrentState = States->ChoosingUnit;
			break;
		}
		case ECombatNotification::AbilityFinished:
		{
			ChangeActiveTeam();
			if (CurrentlyActiveAbility)
			{
				CurrentlyActiveAbility->SetTargeterVisibility(false);
			}
			SetCurrentlyActiveAbility(nullptr);
			CurrentState = States->ChoosingAbility;
			break;
		}
		case ECombatNotification::TargetingCanceled:
		{
			if (CurrentlyActiveAbility)
			{
				CurrentlyActiveAbility->SetTargeterVisibility(false);
			}
			SetCurrentlyActiveAbility(nullptr);
			CurrentState = States->ChoosingAbility;
			break;
		}
	}
	if (CurrentState != StartState)
	{
		StartState->OnExit(this);
		CurrentState->OnEnter(this);
		OnStateChange.Broadcast(CurrentState, StartState);
	}
}

AQETeam* UQECombatStateMachine::GetTeamOne()
{
	return TeamOne;
}

AQETeam* UQECombatStateMachine::GetTeamTwo()
{
	return TeamTwo;
}

UQECombatState* UQECombatStateMachine::GetCurrentState()
{
	return CurrentState;
}

void UQECombatStateMachine::SelectUnit(AQEUnit* Unit)
{
	AQEUnit* OldUnit = SelectedUnit;
	SelectedUnit = Unit;
	OnUnitSelected.Broadcast(Unit, OldUnit);
}

void UQECombatStateMachine::ChangeActiveTeam()
{
	SetActiveTeam(InactiveTeam);
}

AQETeam* UQECombatStateMachine::GetActiveTeam()
{
	return ActiveTeam;
}

void UQECombatStateMachine::SetActiveTeam(AQETeam* NewActiveTeam)
{
	ActiveTeam = NewActiveTeam;
	if (NewActiveTeam == TeamOne)
	{
		InactiveTeam = TeamTwo;
	}
	else
	{
		InactiveTeam = TeamOne;
	}
	OnActiveTeamChange.Broadcast(ActiveTeam, InactiveTeam);
}

void UQECombatStateMachine::SetTeams(AQETeam* NewTeamOne, AQETeam* NewTeamTwo)
{
	TeamOne = NewTeamOne;
	TeamTwo = NewTeamTwo;
	SetActiveTeam(TeamOne);
}

UQEUnitAbility* UQECombatStateMachine::GetCurrentlyActiveAbility()
{
	return CurrentlyActiveAbility;
}

void UQECombatStateMachine::SetCurrentlyActiveAbility(UQEUnitAbility* NewActiveAbility)
{
	CurrentlyActiveAbility = NewActiveAbility;
}

AQEUnit* UQECombatStateMachine::GetSelectedUnit()
{
	return SelectedUnit;
}
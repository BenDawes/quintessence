// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEUnitAbility.h"
#include "Ability_SoothingSap.generated.h"

/**
 *
 */
UCLASS()
class QUINTESSENCE_API UAbility_SoothingSap : public UQEUnitAbility
{
	GENERATED_BODY()
public:
	UAbility_SoothingSap();

	void ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap) override;
};

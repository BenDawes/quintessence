// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "QECombatStates.h"
#include "CS_ChoosingUnit.h"
#include "CS_ChoosingAbility.h"
#include "CS_AbilityResolving.h"
#include "CS_TargetingAbility.h"

UQECombatStates::UQECombatStates()
{
	ChoosingUnit = CreateDefaultSubobject<UCS_ChoosingUnit>(FName("ChoosingUnit"));
	ChoosingAbility = CreateDefaultSubobject<UCS_ChoosingAbility>(FName("ChoosingAbility"));
	AbilityResolving = CreateDefaultSubobject<UCS_AbilityResolving>(FName("AbilityResolving"));
	TargetingAbility = CreateDefaultSubobject<UCS_TargetingAbility>(FName("TargetingAbility"));
}

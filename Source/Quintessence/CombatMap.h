// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FCombatLocator.h"
#include "FCombatGrid.h"
#include "FCombatRow.h"
#include "QEUnit.h"
#include "CombatMap.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API ACombatMap : public AActor
{
	GENERATED_BODY()

protected:
	int TeamOneWidth = 3;
	int TeamOneHeight = 3;
	int TeamTwoWidth = 3;
	int TeamTwoHeight = 3;

public:
	ACombatMap();

	UPROPERTY()
	TArray<AQEUnit*> Units;
	
	UFUNCTION(BlueprintPure)
	int GetTeamWidth(int TeamIndex);

	UFUNCTION(BlueprintPure)
	int GetTeamHeight(int TeamIndex);

	UFUNCTION(BlueprintPure)
	int GetTeamOneWidth();

	UFUNCTION(BlueprintPure)
	int GetTeamOneHeight();

	UFUNCTION(BlueprintPure)
	int GetTeamTwoWidth();

	UFUNCTION(BlueprintPure)
	int GetTeamTwoHeight();

	float CellWidth = 100.f;
	float CellHeight = 100.f;
	
	UFUNCTION()
	FVector GridLocationToRelativeLocationFromLocator(FCombatLocator CombatLocator);
	UFUNCTION()
	FVector GridLocationToRelativeLocation(int TeamIndex, int RowIndex, int ColumnIndexy);

	UFUNCTION()
	FVector GridLocationToWorldLocationFromLocator(FCombatLocator CombatLocator);
	UFUNCTION()
	FVector GridLocationToWorldLocation(int TeamIndex, int RowIndex, int ColumnIndex);
	UFUNCTION()
	FVector GetMapCenter();

	UPROPERTY(VisibleAnywhere)
	FCombatGrid TeamOneGrid;
	UPROPERTY(VisibleAnywhere)
	FCombatGrid TeamTwoGrid;

	FCombatRow* GetRow(int TeamIndex, int RowIndex);
	FCombatRow* GetRow(FCombatLocator CombatLocator);

	FCombatCell* GetCell(int TeamIndex, int RowIndex, int ColumnIndex);
	FCombatCell* GetCell(FCombatLocator CombatLocator);

	TArray<AQEUnit*> GetUnits(int TeamIndex, int RowIndex, int ColumnIndex);
	TArray<AQEUnit*> GetUnits(FCombatLocator CombatLocator);

	void AddUnit(AQEUnit* Unit, FCombatLocator CombatLocator);
	void AddUnit(AQEUnit* Unit, int TeamIndex, int RowIndex, int ColumnIndex);

	void RemoveUnit(AQEUnit* Unit);

	void MoveUnit(AQEUnit* Unit, FCombatLocator NewLocation);


private:

	UPROPERTY(VisibleAnywhere)
	TArray<UStaticMeshComponent*> CellMeshes;

	void SpawnGrid(int TeamIndex, float Width, float Height, FVector Offset);
	void SpawnCells();

	UPROPERTY()
	UStaticMesh* CellMesh;
};

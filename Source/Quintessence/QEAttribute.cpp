// Fill out your copyright notice in the Description page of Project Settings.


#include "QEAttribute.h"

void UQEAttribute::SetBaseValue(float NewBaseValue)
{
	BaseValue = NewBaseValue;
}

void UQEAttribute::SetCurrentValue(float NewCurrentValue)
{
	CurrentValue = NewCurrentValue;
}

void UQEAttribute::SetValues(float NewCurrentValue, float NewBaseValue)
{
	SetCurrentValue(NewCurrentValue);
	SetBaseValue(NewBaseValue);
}

float UQEAttribute::GetBaseValue() const
{
	return BaseValue;
}

float UQEAttribute::GetCurrentValue() const
{
	return CurrentValue;
}

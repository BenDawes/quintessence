// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FCombatCell.h"
#include "FCombatRow.generated.h"

USTRUCT(BlueprintType)
struct QUINTESSENCE_API FCombatRow
{
    GENERATED_BODY()

    FCombatRow() {}
    FCombatRow(int Width) {
        for (int i = 0; i < Width; i++)
        {
            Columns.Emplace(i, FCombatCell());
        }
    }

    UPROPERTY()
    TMap<int, FCombatCell> Columns;
};
// Fill out your copyright notice in the Description page of Project Settings.


#include "Ability_BarkSkin.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEAttribute.h"
#include "QEUnit.h"


UAbility_BarkSkin::UAbility_BarkSkin()
{
	AbilityCost = 10.0f;
	AbilityName = FString("Bark Skin");
	AbilityDescription = FString("Raises defense");
	ConfigureSingleAllyTargeter();
}

void UAbility_BarkSkin::ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap)
{
	Super::ApplyToCombat(SourceUnit, CombatMap);
	TArray<FCombatCell> TargetCells = GetAllTargetedCells(CombatMap);
	for (FCombatCell TargetCell : TargetCells)
	{
		for (AQEUnit* Target : TargetCell.Units)
		{
			UQEAttribute* DefenseBase = Target->Attributes->GetDefenseBase();
			DefenseBase->SetCurrentValue(DefenseBase->GetCurrentValue() + .5f);
		}
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ControlType.h"
#include "GameFramework/GameStateBase.h"
#include "QETeam.h"
#include "QEUnit.h"
#include "QEGameState.generated.h"

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnControlTypeChangeDelegate, EControlType, NewControlType, EControlType, OldControlType);
/**
 * 
 */
UCLASS()
class QUINTESSENCE_API AQEGameState : public AGameStateBase
{
	GENERATED_BODY()

protected:
	UPROPERTY()
	EControlType ControlType = EControlType::Overworld;
public:
	UFUNCTION(BlueprintPure)
	EControlType GetControlType();

	UFUNCTION(BlueprintCallable)
	void SetControlType(EControlType NewControlType);

	UPROPERTY(BlueprintAssignable)
	FOnControlTypeChangeDelegate OnControlTypeChange;
};

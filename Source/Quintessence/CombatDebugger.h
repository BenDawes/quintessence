// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QEUnit.h"
#include "QECombatController.h"
#include "CombatDebugger.generated.h"


UCLASS()
class QUINTESSENCE_API ACombatDebugger : public AActor
{
	GENERATED_BODY()
private:
	TSubclassOf<UUserWidget> UIClass;
	UUserWidget* PlayerUI;

public:
	ACombatDebugger();

	virtual void BeginPlay() override;
	UPROPERTY()
		AQECombatController* CombatController;

	UPROPERTY()
		AQETeam* TeamOne;
	UPROPERTY()
		AQETeam* TeamTwo;

	UPROPERTY()
		TArray<AQEUnit*> TeamOneArr;

	UPROPERTY()
		TArray<AQEUnit*> TeamTwoArr;

	UPROPERTY()
		TArray<AQEUnit*> OldTeamOneArr;

	UPROPERTY()
		TArray<AQEUnit*> OldTeamTwoArr;

	UFUNCTION(BlueprintCallable)
		void AddUnit(int TeamIndex, AQEUnit* Unit);

	UFUNCTION(BlueprintCallable)
		void StartCombat();

	UFUNCTION(BlueprintCallable)
		void InitializeCombatController();

	
};
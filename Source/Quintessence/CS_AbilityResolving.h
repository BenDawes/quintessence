// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QECombatState.h"
#include "QECombatStateMachine.h"
#include "CS_AbilityResolving.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UCS_AbilityResolving : public UQECombatState
{
	GENERATED_BODY()
public:
	UCS_AbilityResolving();

	UPROPERTY()
	UQECombatStateMachine* StoredCombat;

	UPROPERTY()
	FTimerHandle FinishedResolvingHandle;

	UFUNCTION()
	void OnFinishedResolving();
	
	virtual void OnEnter(UQECombatStateMachine* Combat) override;
	virtual void OnExit(UQECombatStateMachine* Combat) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "QEUnitAbility.h"
#include "QEUnitAbilities.generated.h"

/**
 *
 */
UCLASS()
class QUINTESSENCE_API UQEUnitAbilities : public UObject
{
	GENERATED_BODY()
public:
	UQEUnitAbilities();
protected:
	UPROPERTY()
	TArray<UQEUnitAbility*> Abilities;
public:
	UFUNCTION(BlueprintPure)
	UQEUnitAbility* GetAbilityByLocator(EAbilityLocator Locator);

	UFUNCTION(BlueprintPure)
	int GetIndexFromLocator(EAbilityLocator Locator);

	UFUNCTION(BlueprintCallable)
	void GainAbility(UQEUnitAbility* Ability);
};
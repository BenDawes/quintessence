// Fill out your copyright notice in the Description page of Project Settings.


#include "Unit_Twiglet.h"
#include "Ability_Bite.h"
#include "Ability_ThornGraze.h"

AUnit_Twiglet::AUnit_Twiglet()
{
	GainAbility(CreateDefaultSubobject<UAbility_ThornGraze>(FName("AbilityThornGraze")));
	GainAbility(CreateDefaultSubobject<UAbility_Bite>(FName("AbilityBite")));
	AssignSkeleton(TEXT("SkeletalMesh'/Game/AnimalVarietyPack/Crow/Meshes/SK_Crow.SK_Crow'"));
}

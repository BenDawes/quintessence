// Fill out your copyright notice in the Description page of Project Settings.

#include "CT_SingleAlly.h"
#include "CombatMap.h"
#include "FCombatCell.h"

ACT_SingleAlly::ACT_SingleAlly()
{
    CenterLocator = FCombatLocator(0, 0, 0);
}

TArray<FCombatCell> ACT_SingleAlly::GetAllCells(ACombatMap* CombatMap)
{
    TArray<FCombatCell> Result = TArray<FCombatCell>();
    Result.Add(*CombatMap->GetCell(CenterLocator));
    return Result;
}

void ACT_SingleAlly::Move(EAbilityLocator Direction, ACombatMap* CombatMap)
{
    FCombatLocator NewLocator = UpdateLocatorUnchecked(CenterLocator, CombatMap, Direction);
    CenterLocator = ClampLocator(NewLocator, CombatMap);
    PostMove(CombatMap);
}

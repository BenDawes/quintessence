// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "QECombatStateMachine.h"
#include "QETeam.h"
#include "MoveSelectorCPP.h"
#include "CombatHUD.generated.h"

class AQECombatController;
/**
 * 
 */
UCLASS()
class QUINTESSENCE_API ACombatHUD : public AHUD
{
	GENERATED_BODY()

public:
	ACombatHUD();
	virtual void DrawHUD() override;

	void SetCombatStateMachine(UQECombatStateMachine* NewState);
	void SetCombatController(AQECombatController* NewCombatController);

	virtual void BeginPlay() override;

private:

	UPROPERTY()
	UQECombatStateMachine* CombatState;

	UPROPERTY()
	AQECombatController* CombatController;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<class UUserWidget> AbilitiesWidgetClass;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	class UMoveSelectorCPP* CurrentAbilitiesWidget;

	UFUNCTION()
	void OnUnitSelected(AQEUnit* NewUnit, AQEUnit* OldUnit);
public:
	void BindToExternalInputComponent(UInputComponent* ExternalInputComponent);

	void SetOwningTeam(AQETeam* Team);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "QEPlayerController.h"
#include "CombatDebugger.h"
#include "CombatHUD.h"
#include "QECombatController.h"
#include "QEGameState.h"
#include "Kismet/GameplayStatics.h"

AQEPlayerController::AQEPlayerController()
{
}

void AQEPlayerController::BeginPlay()
{
	Super::BeginPlay();
	FActorSpawnParameters Params = FActorSpawnParameters();
	Params.Name = FName("PC_CombatController");
	if (UWorld* World = GetWorld())
	{
		CombatController = World->SpawnActor<AQECombatController>(AQECombatController::StaticClass(), Params);
		AQEGameState* GameState = World->GetGameState<AQEGameState>();
		GameState->OnControlTypeChange.AddDynamic(this, &AQEPlayerController::OnControlTypeChange);
	}
}

void AQEPlayerController::AcknowledgePossession(APawn* NewPawn)
{
	Super::AcknowledgePossession(NewPawn);
	AQETeam* AsTeam = Cast<AQETeam>(NewPawn);
	if (AsTeam)
	{
		TArray<AQEUnit*> Units = AsTeam->GetUnits();
		if (Units.Num() > 0)
		{
			CombatController->SelectUnit(Units[0]);
		}
		ACombatHUD* HUD = GetHUD<ACombatHUD>();
		CombatController->SetHUD(HUD);
		HUD->SetOwningTeam(AsTeam);
		HUD->BindToExternalInputComponent(InputComponent);
	}
}

void AQEPlayerController::OnControlTypeChange(EControlType NewType, EControlType OldType)
{
	UWorld* World = GetWorld();
	if (World == nullptr)
	{
		return;
	}
	switch (OldType)
	{
		case EControlType::CombatDebugging:
		{
			if (NewType != EControlType::CombatDebugging)
			{
				if (CombatDebugger != nullptr)
				{
					CombatDebugger->Destroy();
				}
			}
		}
	}
	switch (NewType)
	{
		case EControlType::Overworld:
		{

		}
		case EControlType::Combat:
		{

		}
		case EControlType::CombatDebugging:
		{
			CombatDebugger = World->SpawnActor<ACombatDebugger>();
		}
		case EControlType::Disabled:
		default:
		{

		}
	}
	ControlType = NewType;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "QEAttribute.h"
#include "AttributePercentageBarCPP.generated.h"

/**
 *
 */
UCLASS()
class QUINTESSENCE_API UAttributePercentageBarCPP : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY()
	UQEAttribute* Attribute;

	UPROPERTY()
	UQEAttribute* MaxAttribute;
public:
	UFUNCTION(BlueprintPure)
	UQEAttribute* GetAttribute();

	UFUNCTION()
	void SetAttribute(UQEAttribute* NewAttribute);
	
	UFUNCTION(BlueprintPure)
	UQEAttribute* GetMaxAttribute();

	UFUNCTION()
	void SetMaxAttribute(UQEAttribute* NewAttribute);
};
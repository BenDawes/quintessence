// Fill out your copyright notice in the Description page of Project Settings.

#include "Ability_Cauterize.h"
#include "CombatMap.h"
#include "FCombatLocator.h"
#include "QEAttribute.h"
#include "QEUnit.h"


UAbility_Cauterize::UAbility_Cauterize()
{
	AbilityCost = 10.0f;
	AbilityName = FString("Cauterize");
	AbilityDescription = FString("Heals a single target while raising defense");
	ConfigureSingleAllyTargeter();
}

void UAbility_Cauterize::ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap)
{
	Super::ApplyToCombat(SourceUnit, CombatMap);
	TArray<FCombatCell> TargetCells = GetAllTargetedCells(CombatMap);
	for (FCombatCell TargetCell : TargetCells)
	{
		for (AQEUnit* Target : TargetCell.Units)
		{
			UQEAttribute* DefenseBase = Target->Attributes->GetDefenseBase();
			DefenseBase->SetCurrentValue(DefenseBase->GetCurrentValue() + .2f);

			UQEAttribute* Health = Target->Attributes->GetHealth();
			Health->SetCurrentValue(Health->GetCurrentValue() + 5.f);
		}
	}
}
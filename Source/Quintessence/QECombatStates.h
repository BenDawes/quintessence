// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QECombatState.h"
#include "QECombatStates.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API UQECombatStates : public UObject
{
	GENERATED_BODY()
public:
	UQECombatStates();
	UPROPERTY()
	UQECombatState* ChoosingUnit;
	UPROPERTY()
	UQECombatState* ChoosingAbility;
	UPROPERTY()
	UQECombatState* AbilityResolving;
	UPROPERTY()
	UQECombatState* TargetingAbility;
};

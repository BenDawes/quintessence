// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ControlType.generated.h"

UENUM(BlueprintType)
enum class EControlType : uint8
{
	Overworld			UMETA(DisplayName = "Overworld"),
	Combat				UMETA(DisplayName = "Combat"),
	CombatDebugging     UMETA(DisplayName = "CombatDebugging"),
	Disabled			UMETA(DisplayName = "Disabled")
};

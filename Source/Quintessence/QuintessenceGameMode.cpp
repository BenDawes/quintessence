// Copyright Epic Games, Inc. All Rights Reserved.

#include "QuintessenceGameMode.h"
#include "QuintessenceCharacter.h"
#include "CombatHud.h"
#include "QEPlayerController.h"
#include "QEGameState.h"
#include "UObject/ConstructorHelpers.h"

AQuintessenceGameMode::AQuintessenceGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	HUDClass = ACombatHUD::StaticClass();

	GameStateClass = AQEGameState::StaticClass();

	PlayerControllerClass = AQEPlayerController::StaticClass();
}

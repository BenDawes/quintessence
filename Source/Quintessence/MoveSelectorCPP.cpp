// Fill out your copyright notice in the Description page of Project Settings.


#include "MoveSelectorCPP.h"
#include "QECombatController.h"
#include "QECombatStateMachine.h"
#include "QETeam.h"
#include "QEUnit.h"
#include "QEUnitAbilities.h"
#include "QEUnitAbility.h"
#include "AbilityLocator.h"

UQEUnitAbility* UMoveSelectorCPP::GetAbility(EAbilityLocator Locator)
{
	return Unit->GetAbilities()->GetAbilityByLocator(Locator);
}

void UMoveSelectorCPP::SetOwningTeam(AQETeam* Team)
{
	OwningTeam = Team;
}

AQETeam* UMoveSelectorCPP::GetActiveTeam()
{
	if (!CombatController)
	{
		return nullptr;
	}
	UQECombatStateMachine* Combat = CombatController->GetCombat();
	if (!Combat)
	{
		return nullptr;
	}
	return Combat->GetActiveTeam();
}

FString UMoveSelectorCPP::GetLargeText()
{
	if (!CombatController)
	{
		return FString();
	}
	UQECombatStateMachine* Combat = CombatController->GetCombat();
	if (!Combat)
	{
		return FString();
	}
	EQECombatStateType StateType = Combat->GetCurrentState()->StateType;
	switch (StateType)
	{
		case EQECombatStateType::ChoosingUnit:
		{
			if (OwningTeam == GetActiveTeam())
			{
				AQEUnit* TargetedUnit = GetTargetedUnitForSelection();
				if (TargetedUnit)
				{
					return TargetedUnit->GetName();
				}
				return FString();
			}
			return FString("Enemy turn...");
		}
		case EQECombatStateType::ChoosingAbility:
		{
			if (OwningTeam == GetActiveTeam())
			{
				return FString();
			}
			return FString("Enemy turn...");
		}
		case EQECombatStateType::TargetingAbility:
		{
			return GetAbility(SelectedAction)->AbilityDescription;
		}
		case EQECombatStateType::AbilityResolving:
		{
			return FString("Ability resolving...");
		}
		default:
			return FString();
	}
}

AQEUnit* UMoveSelectorCPP::GetTargetedUnitForSelection()
{
	TArray<FCombatCell> TargetedCells = CombatController->GetTargeter()->GetAllCells(CombatController->GetCombatMap());
	if (TargetedCells.Num() == 0)
	{
		return nullptr;
	}
	TArray<AQEUnit*> TargetedUnits = TargetedCells[0].Units;
	if (TargetedUnits.Num() == 0)
	{
		return nullptr;
	}
	return TargetedUnits[0];
}

void UMoveSelectorCPP::SelectUnit()
{
	CombatController->SelectUnit(GetTargetedUnitForSelection());
}

void UMoveSelectorCPP::BeginTargetingAbility(EAbilityLocator Locator)
{
	SelectedAction = Locator;
	CombatController->BeginTargetingAbility(Unit, Locator);
}

void UMoveSelectorCPP::EnactAbility(EAbilityLocator Locator)
{
	SelectedAction = EAbilityLocator::None;
	CombatController->ConfirmAbility(Unit, Locator);
}

void UMoveSelectorCPP::CancelTargetingAbility()
{
	SelectedAction = EAbilityLocator::None;
	CombatController->StopTargeting();
}

EQECombatStateType UMoveSelectorCPP::GetCombatStateType()
{
	if (!CombatController)
	{
		return EQECombatStateType::ChoosingAbility;
	}
	UQECombatStateMachine* Combat = CombatController->GetCombat();
	if (!Combat)
	{
		return EQECombatStateType::ChoosingAbility;
	}
	return Combat->GetCurrentState()->StateType;
}

void UMoveSelectorCPP::SetCombatController(AQECombatController* NewCombatController)
{
	CombatController = NewCombatController;
}

void UMoveSelectorCPP::BindToExternalInputComponent(UInputComponent* ExternalInputComponent)
{
	ExternalInputComponent->BindAction(FName("FourWayUILeft"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Left, false>);
	ExternalInputComponent->BindAction(FName("FourWayUITop"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Top, false>);
	ExternalInputComponent->BindAction(FName("FourWayUIRight"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Right, false>);
	ExternalInputComponent->BindAction(FName("FourWayUIBottom"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Bottom, false>);

	ExternalInputComponent->BindAction(FName("FourWayUILeft"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Left, false>);
	ExternalInputComponent->BindAction(FName("FourWayUITop"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Top, false>);
	ExternalInputComponent->BindAction(FName("FourWayUIRight"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Right, false>);
	ExternalInputComponent->BindAction(FName("FourWayUIBottom"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Bottom, false>);

	ExternalInputComponent->BindAction(FName("FourWayUILeftAlt"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Left, true>);
	ExternalInputComponent->BindAction(FName("FourWayUITopAlt"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Top, true>);
	ExternalInputComponent->BindAction(FName("FourWayUIRightAlt"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Right, true>);
	ExternalInputComponent->BindAction(FName("FourWayUIBottomAlt"), EInputEvent::IE_Pressed, this, &UMoveSelectorCPP::InputPressed<EAbilityLocator::Bottom, true>);

	ExternalInputComponent->BindAction(FName("FourWayUILeftAlt"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Left, true>);
	ExternalInputComponent->BindAction(FName("FourWayUITopAlt"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Top, true>);
	ExternalInputComponent->BindAction(FName("FourWayUIRightAlt"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Right, true>);
	ExternalInputComponent->BindAction(FName("FourWayUIBottomAlt"), EInputEvent::IE_Released, this, &UMoveSelectorCPP::InputReleased<EAbilityLocator::Bottom, true>);
}

template<EAbilityLocator Locator, bool bWasAlt>
void UMoveSelectorCPP::InputPressed()
{
	InputPressed(Locator, bWasAlt);
}

template<EAbilityLocator Locator, bool bWasAlt>
void UMoveSelectorCPP::InputReleased()
{
	InputReleased(Locator, bWasAlt);
}

void UMoveSelectorCPP::InputPressed(EAbilityLocator Locator, bool bWasAlt)
{
}

void UMoveSelectorCPP::InputReleased(EAbilityLocator Locator, bool bWasAlt)
{
	InputConfirmed(Locator, bWasAlt);
}

void UMoveSelectorCPP::InputConfirmed(EAbilityLocator Locator, bool bWasAlt)
{
	if (!CombatController)
	{
		return;
	}
	UQECombatStateMachine* Combat = CombatController->GetCombat();
	if (!Combat)
	{
		return;
	}
	EQECombatStateType StateType = Combat->GetCurrentState()->StateType;
	switch (StateType)
	{
		case EQECombatStateType::ChoosingUnit:
		{
			if (bWasAlt)
			{
				CombatController->MoveTargeter(Locator);
				return;
			}
			if (Locator == EAbilityLocator::Bottom)
			{
				SelectUnit();
			}
		}
		case EQECombatStateType::ChoosingAbility:
		{
			if (bWasAlt)
			{
				return;
			}
			BeginTargetingAbility(Locator);
			return;
		}
		case EQECombatStateType::TargetingAbility:
		{
			if (bWasAlt)
			{
				CombatController->MoveTargeter(Locator);
			}
			else
			{
				if (Locator == SelectedAction)
				{
					EnactAbility(Locator);
				}
				else
				{
					CancelTargetingAbility();
				}
			}
			return;
		}
		default:
			return;
	}
}

void UMoveSelectorCPP::InputCancelled()
{
	if (!CombatController)
	{
		return;
	}
	UQECombatStateMachine* Combat = CombatController->GetCombat();
	if (!Combat)
	{
		return;
	}
	if (Combat->GetCurrentState()->StateType == EQECombatStateType::TargetingAbility)
	{
		CancelTargetingAbility();
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QEUnit.h"
#include "Unit_FireSprite.generated.h"

/**
 * 
 */
UCLASS()
class QUINTESSENCE_API AUnit_FireSprite : public AQEUnit
{
	GENERATED_BODY()
public:
	AUnit_FireSprite();
};

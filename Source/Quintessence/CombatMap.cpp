// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatMap.h"
#include "Engine/EngineTypes.h"
#include "FCombatCell.h"
#include "FCombatRow.h"
#include "FCombatLocator.h"
#include "FCombatGrid.h"

ACombatMap::ACombatMap()
{
    TeamOneGrid = FCombatGrid(TeamOneHeight, TeamOneWidth);
    TeamTwoGrid = FCombatGrid(TeamTwoHeight, TeamTwoWidth);
    static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube'"));
    if (MeshAsset.Succeeded())
    {
        CellMesh = MeshAsset.Object;
    }
    RootComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
    SpawnCells();
}

int ACombatMap::GetTeamWidth(int TeamIndex)
{
    return TeamIndex == 0 ? GetTeamOneWidth() : GetTeamTwoWidth();
}

int ACombatMap::GetTeamHeight(int TeamIndex)
{
    return TeamIndex == 0 ? GetTeamOneHeight() : GetTeamTwoHeight();
}

int ACombatMap::GetTeamOneWidth()
{
    return TeamOneWidth;
}

int ACombatMap::GetTeamOneHeight()
{
    return TeamOneHeight;
}

int ACombatMap::GetTeamTwoWidth()
{
    return TeamTwoWidth;
}

int ACombatMap::GetTeamTwoHeight()
{
    return TeamTwoHeight;
}

FVector ACombatMap::GridLocationToRelativeLocationFromLocator(FCombatLocator CombatLocator)
{
    return GridLocationToRelativeLocation(CombatLocator.TeamIndex, CombatLocator.RowIndex, CombatLocator.ColumnIndex);
}

FVector ACombatMap::GridLocationToRelativeLocation(int TeamIndex, int RowIndex, int ColumnIndex)
{
    if (TeamIndex == 0)
    {
        return FVector(ColumnIndex * CellWidth, RowIndex * CellHeight, 100);
    }
    else
    {
        FVector TeamTwoGridStart = FVector((TeamTwoWidth - TeamOneWidth) * CellWidth / 2, (TeamOneHeight + 3) * CellWidth, 0);
        return TeamTwoGridStart + FVector(ColumnIndex * CellWidth, RowIndex * CellHeight, 100);
    }
}

FVector ACombatMap::GridLocationToWorldLocationFromLocator(FCombatLocator CombatLocator)
{
    return GridLocationToWorldLocation(CombatLocator.TeamIndex, CombatLocator.RowIndex, CombatLocator.ColumnIndex);
}

FVector ACombatMap::GridLocationToWorldLocation(int TeamIndex, int RowIndex, int ColumnIndex)
{
    FVector ActorLoc = GetActorLocation();
    return GridLocationToRelativeLocation(TeamIndex, RowIndex, ColumnIndex) + ActorLoc;
}

FVector ACombatMap::GetMapCenter()
{
    return GetActorLocation() + FVector(CellWidth * TeamOneWidth / 2, CellHeight * (TeamOneHeight + 1.5), 100);
}

FCombatRow* ACombatMap::GetRow(int TeamIndex, int RowIndex)
{
    if (TeamIndex == 0)
    {
        return TeamOneGrid.Rows.Find(RowIndex);
    }
    if (TeamIndex == 1)
    {
        return TeamTwoGrid.Rows.Find(RowIndex);
    }
    return nullptr;
}

FCombatRow* ACombatMap::GetRow(FCombatLocator CombatLocator)
{
    return GetRow(CombatLocator.TeamIndex, CombatLocator.RowIndex);
}

FCombatCell* ACombatMap::GetCell(int TeamIndex, int RowIndex, int ColumnIndex)
{
    return GetRow(TeamIndex, RowIndex)->Columns.Find(ColumnIndex);
}

FCombatCell* ACombatMap::GetCell(FCombatLocator CombatLocator)
{
    return GetCell(CombatLocator.TeamIndex, CombatLocator.RowIndex, CombatLocator.ColumnIndex);
}

TArray<AQEUnit*> ACombatMap::GetUnits(int TeamIndex, int RowIndex, int ColumnIndex)
{
    FCombatCell* Cell = GetCell(TeamIndex, RowIndex, ColumnIndex);
    if (Cell != nullptr)
    {
        return Cell->Units;
    }
    return TArray<AQEUnit*>();
}

TArray<AQEUnit*> ACombatMap::GetUnits(FCombatLocator CombatLocator)
{
    return GetUnits(CombatLocator.TeamIndex, CombatLocator.RowIndex, CombatLocator.ColumnIndex);
}

void ACombatMap::AddUnit(AQEUnit* Unit, FCombatLocator CombatLocator)
{
    AddUnit(Unit, CombatLocator.TeamIndex, CombatLocator.RowIndex, CombatLocator.ColumnIndex);
}

void ACombatMap::AddUnit(AQEUnit* Unit, int TeamIndex, int RowIndex, int ColumnIndex)
{
    FCombatLocator NewLocation = FCombatLocator(TeamIndex, RowIndex, ColumnIndex);
    Unit->CombatLocator = NewLocation;
    Unit->SetActorLocation(GridLocationToWorldLocationFromLocator(NewLocation));
    FCombatCell* Cell = GetCell(TeamIndex, RowIndex, ColumnIndex);
    if (Cell != nullptr)
    {
        Cell->Units.Add(Unit);
    }
}

void ACombatMap::RemoveUnit(AQEUnit* Unit)
{
    if (Unit)
    {
        FCombatCell* Cell = GetCell(Unit->CombatLocator);
        if (Cell != nullptr)
        {
            Cell->Units.Remove(Unit);
        }
    }
}

void ACombatMap::MoveUnit(AQEUnit* Unit, FCombatLocator NewLocation)
{
    RemoveUnit(Unit);
    AddUnit(Unit, NewLocation);
}

void ACombatMap::SpawnGrid(int TeamIndex, float Width, float Height, FVector Offset)
{
    for (int i = 0; i < Height; i++)
    {
        for (int j = 0; j < Width; j++)
        {
            UStaticMeshComponent* StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName(*FString::Printf(TEXT("StaticMesh{%s,%s,%s}"), *FString::FromInt(TeamIndex), *FString::FromInt(i), *FString::FromInt(j))));
            StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
            CellMeshes.Add(StaticMesh);
            FVector Loc = FVector(j * CellWidth, i * CellHeight, 0);
            StaticMesh->SetRelativeLocation(Loc + Offset);
            if (CellMesh)
            {
                StaticMesh->SetStaticMesh(CellMesh);
            }
        }
    }
}

void ACombatMap::SpawnCells()
{
    FVector TeamOneGridOffset = FVector(0, 0, 0);
    FVector TeamTwoGridOffset = FVector((TeamTwoWidth - TeamOneWidth) * CellWidth / 2, (TeamOneHeight + 3) * CellWidth, 0);
    SpawnGrid(0, TeamOneWidth, TeamOneHeight, TeamOneGridOffset);
    SpawnGrid(1, TeamTwoWidth, TeamTwoHeight, TeamTwoGridOffset);
}



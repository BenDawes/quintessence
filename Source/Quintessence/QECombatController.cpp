// Fill out your copyright notice in the Description page of Project Settings.


#include "QECombatController.h"
#include "AbilityLocator.h"
#include "CombatMap.h"
#include "CombatHUD.h"
#include "CT_SingleAlly.h"
#include "QECombatState.h"
#include "QEUnit.h"
#include "QEUnitAbilities.h"
#include "QEUnitAbility.h"
#include "QEGameState.h"
#include "Engine/World.h"
#include "CombatNotification.h"

AQECombatController::AQECombatController()
{
	Combat = CreateDefaultSubobject<UQECombatStateMachine>(FName("Combat"));
	TargeterComp = CreateDefaultSubobject<UChildActorComponent>(FName("CombatTargeter"));
	TargeterComp->SetChildActorClass(ACT_SingleAlly::StaticClass());
	TargeterComp->SetVisibility(false, true);
}

void AQECombatController::BeginPlay()
{
	Super::BeginPlay();
	if (UWorld* World = GetWorld())
	{
		CombatMap = World->SpawnActor<ACombatMap>(ACombatMap::StaticClass(), FVector(100, 100, 400), FRotator::ZeroRotator);
	}
}

void AQECombatController::StartCombat()
{
	UE_LOG(LogTemp, Warning, TEXT("Starting combat"));
	Combat->OnStateChange.AddDynamic(this, &AQECombatController::OnStateChange);
	Combat->OnActiveTeamChange.AddDynamic(this, &AQECombatController::OnActiveTeamChange);

	AQETeam* TeamOne = Combat->GetTeamOne();
	if (UWorld* World = GetWorld())
	{
		World->GetFirstPlayerController()->Possess(TeamOne);
	}
	Combat->SetActiveTeam(TeamOne);
	Combat->Start();
}

void AQECombatController::SelectUnit(AQEUnit* Unit)
{
	Combat->SelectUnit(Unit);
	Combat->Notify(ECombatNotification::UnitSelected);
}

void AQECombatController::BeginTargetingAbility(AQEUnit* SourceUnit, EAbilityLocator Locator)
{
	UQEUnitAbility* Ability = SourceUnit->GetAbilities()->GetAbilityByLocator(Locator);
	if (Ability == nullptr)
	{
		return;
	}
	Combat->SetCurrentlyActiveAbility(Ability);
	UE_LOG(LogTemp, Warning, TEXT("BeginTargetingAbility Ability was %s"), *Ability->GetName());
	Ability->UpdateTargeter(CombatMap);
	Combat->Notify(ECombatNotification::TargetingStarted);
}

void AQECombatController::ConfirmAbility(AQEUnit* SourceUnit, EAbilityLocator Locator)
{
	UQEUnitAbility* Ability = SourceUnit->GetAbilities()->GetAbilityByLocator(Locator);
	if (Ability == nullptr)
	{
		return;
	}
	Ability->ApplyToCombat(SourceUnit, CombatMap);
	Combat->Notify(ECombatNotification::AbilityConfirmed);
}

void AQECombatController::StopTargeting()
{
	Combat->Notify(ECombatNotification::TargetingCanceled);
}

void AQECombatController::SetTeams(AQETeam* TeamOne, AQETeam* TeamTwo)
{
	FVector Center = CombatMap->GetMapCenter();
	TeamOne->SetActorLocation(Center);
	TeamTwo->SetActorLocation(Center);
	Combat->SetTeams(TeamOne, TeamTwo);
}

void AQECombatController::OnStateChange(UQECombatState* NewState, UQECombatState* OldState)
{

}

void AQECombatController::OnActiveTeamChange(AQETeam* NewActiveTeam, AQETeam* NewInactiveTeam)
{
	ACombatTargeter* Targeter = GetTargeter();
	if (NewActiveTeam == Combat->GetTeamOne())
	{
		Targeter->CenterLocator.TeamIndex = 0;
	}
	else
	{
		Targeter->CenterLocator.TeamIndex = 1;
	}
	Targeter->PostMove(CombatMap);
}

void AQECombatController::SetHUD(ACombatHUD* NewHUD)
{
	NewHUD->SetCombatStateMachine(Combat);
	NewHUD->SetCombatController(this);
}

ACombatMap* AQECombatController::GetCombatMap()
{
	return CombatMap;
}

UQECombatStateMachine* AQECombatController::GetCombat()
{
	return Combat;
}

void AQECombatController::MoveTargeter(EAbilityLocator Direction)
{
	EQECombatStateType CurrentState = Combat->GetCurrentState()->StateType;
	switch (CurrentState)
	{
		case EQECombatStateType::TargetingAbility:
		{
			UQEUnitAbility* CurrentlyActiveAbility = Combat->GetCurrentlyActiveAbility();
			if (!CurrentlyActiveAbility)
			{
				return;
			}
			CurrentlyActiveAbility->MoveTargeter(Direction, CombatMap);
		}
		case EQECombatStateType::ChoosingAbility:
		{
			GetTargeter()->Move(Direction, CombatMap);
		}
		default:
			return;
	}
}

ACombatTargeter* AQECombatController::GetTargeter()
{
	if (CombatTargeter)
	{
		return CombatTargeter;
	}
	CombatTargeter = Cast<ACombatTargeter>(TargeterComp->GetChildActor());
	return CombatTargeter;
}

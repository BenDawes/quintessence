// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatMap.h"
#include "FCombatCell.h"
#include "CombatTargeter.h"
#include "CT_SingleAlly.generated.h"

/**
 *
 */
UCLASS()
class QUINTESSENCE_API ACT_SingleAlly : public ACombatTargeter
{
	GENERATED_BODY()
public:
	ACT_SingleAlly();

	UFUNCTION(BlueprintCallable)
	virtual TArray<FCombatCell> GetAllCells(ACombatMap* CombatMap) override;

	virtual void Move(EAbilityLocator Direction, ACombatMap* CombatMap) override;
};

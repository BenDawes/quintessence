// Fill out your copyright notice in the Description page of Project Settings.


#include "QEUnitAbility.h"
#include "CombatTargeter.h"
#include "CT_SingleEnemy.h"
#include "CT_SingleAlly.h"

void UQEUnitAbility::ConfigureSingleEnemyTargeter()
{
	TargeterComp->SetChildActorClass(ACT_SingleEnemy::StaticClass());
}

void UQEUnitAbility::ConfigureSingleAllyTargeter()
{
	TargeterComp->SetChildActorClass(ACT_SingleAlly::StaticClass());
}

UQEUnitAbility::UQEUnitAbility()
{
	TargeterComp = CreateDefaultSubobject<UChildActorComponent>(FName("CombatTargeter"));
	TargeterComp->SetVisibility(false, true);
}

void UQEUnitAbility::ApplyToCombat(AQEUnit* SourceUnit, ACombatMap* CombatMap)
{
	UE_LOG(LogTemp, Warning, TEXT("Ability fired!"));
}

void UQEUnitAbility::MoveTargeter(EAbilityLocator Direction, ACombatMap* CombatMap)
{
	ACombatTargeter* CombatTargeter = Cast<ACombatTargeter>(TargeterComp->GetChildActor());
	if (!CombatTargeter)
	{
		return;
	}
	CombatTargeter->Move(Direction, CombatMap);
}

void UQEUnitAbility::UpdateTargeter(ACombatMap* CombatMap)
{
	ACombatTargeter* CombatTargeter = Cast<ACombatTargeter>(TargeterComp->GetChildActor());
	if (!CombatTargeter)
	{
		return;
	}
	CombatTargeter->PostMove(CombatMap);
}

void UQEUnitAbility::SetTargeterVisibility(bool bIsVisible)
{
	if (!TargeterComp)
	{
		return;
	}
	if (bIsVisible)
	{
		TargeterComp->SetVisibility(true, false);
	}
	else
	{
		TargeterComp->SetVisibility(false, true);
	}
}


TArray<FCombatCell> UQEUnitAbility::GetAllTargetedCells(ACombatMap* CombatMap)
{
	ACombatTargeter* CombatTargeter = Cast<ACombatTargeter>(TargeterComp->GetChildActor());
	if (!CombatTargeter)
	{
		return TArray<FCombatCell>();
	}
	return CombatTargeter->GetAllCells(CombatMap);
}
